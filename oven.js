// Take the query string from the url
const queryString = window.location.search;
console.log(queryString);

// Get the ingd= url parameter
const urlParams = new URLSearchParams(queryString);
const ingd = urlParams.get('ingd');
console.log(ingd);

// Set the ingd form input to what was passed in the url
document.getElementById('ingd').value = ingd;

let heatValue = document.getElementById('heat').value;
console.log(heatValue + ' celsius');

let timerValue = document.getElementById('timer').value;
console.log(timerValue + ' minutes');