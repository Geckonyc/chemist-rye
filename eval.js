const queryString = window.location.search;
console.log("queryString = " + queryString);
const urlParams = new URLSearchParams(queryString);
const ingd = urlParams.get('ingd');
const timer = urlParams.get('timer');
const heat = urlParams.get('heat');

console.log("ingd = " + ingd);

let inglist = ingd.split(",");
console.log(inglist);

let out = {};
for (const i in inglist) {
    let ing = inglist[i];
    console.log("i=" + i + " ing=" + ing);
    if (out[ing] == undefined) {
        out[ing] = 1
    } else {
        out[ing] = out[ing] + 1
    }
}
out['heat'] = heat;
out['timer'] = timer;

// TODO: support multiple recipes
let recipe = recipies['sodabread'];

let table = document.getElementById('results');

for (const ing in out) {
    let used = out[ing];
    let wanted = recipe[ing];
    console.log("key=" + ing + " used=" + used, " wanted=" + wanted);
    let name = ingredientNames[ing];

    let msg = "";
    if (used > wanted) {
        msg = "Too much " + name;
    } else if (used < wanted) {
        msg = "Too little " + name;
    } else if (used == wanted) {
        msg = "Perfect " + name;
    }
    console.log(msg);

    let row = document.createElement('TR');
    let cell = document.createElement('TD');
    cell.appendChild(document.createTextNode(msg));
    
    let button = document.createElement("BUTTON");
    button.innerHTML = "Expand";
    cell.appendChild(button);
    let attr = document.createAttribute("onclick");
    attr.value = "onExpandClicked('" + ing + "')";
    button.setAttributeNode(attr);

    row.appendChild(cell);
    table.appendChild(row);
}

function onExpandClicked(ingr) {
    console.log('ingr=' + ingr);
    let expl = ingredientExpl[ingr];
    alert(expl);
  }