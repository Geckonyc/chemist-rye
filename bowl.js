const MAXING = 20;

document.getElementById('app').innerHTML = ``;
let ingredients = [];
let bowl = document.getElementById('bowl')
let bowlheight = bowl.clientHeight;
let bowlwidth = bowl.clientWidth;
let imgheight = bowlheight / MAXING;
let yoffset = imgheight * (MAXING-1);
console.log("bowlheight=" + bowlheight + " imgheight=" + imgheight);

const queryString = window.location.search;
console.log(queryString);
const urlParams = new URLSearchParams(queryString);
const ingd = urlParams.get('ingd');
console.log(ingd);

function ingredientClicked(ingname) {
  ingredients.push(ingname);

  let canvas = document.getElementById('bowl');
  let ctx = canvas.getContext('2d');
  let img = document.getElementById(ingname);
  ctx.drawImage(img, 0, yoffset, bowlwidth, imgheight);

  yoffset = yoffset - imgheight;

  console.log('ingredients = ' + ingredients);
  console.log(yoffset);
  document.getElementById('ingd').value = ingredients
}

function clearClicked() {
  ingredients = [];
  let canvas = document.getElementById('bowl');
  let ctx = canvas.getContext('2d');
  ctx.clearRect(0, 0, canvas.width, canvas.height);

  yoffset = imgheight * (MAXING-1);
  console.log(yoffset);
}