let recipies = {
    "sodabread": {
        flour: 5, h2o: 4, bs: 2, nac1: 1,
        heat: 235,
        timer: 35,
    },
    // TODO: add new recipes here!
};

let ingredientNames = {
    nac1: 'Salt',
    bs: 'Baking Soda',
    h2o: 'Water',
    flour: 'Flour',
    heat: 'Heat',
    timer: 'Time',
};

let ingredientExpl = {
    nac1: 'Salt is a naturally occurring mineral called Sodium Chloride. It is found and harvested from the oceans and deposits in the earth’s crust. Salt can be used in a lot of ways, but it’s most commonly used in cooking. Salt is used from everything from sauces to soups and even breads. Bread uses salt to strengthen the chemical bonds in the dough, as well as making it taste better once it’s done. Too little salt will make a dough that rises too fast, and the bread wont have structure, flavor or crust color. Too much salt in the dough will slow down or even stop the bread from rising.',
    bs: 'Baking soda is a chemical compound used in a variety of ways, in fact, you can use baking soda for just about anything. Baking soda can be used as toothpaste, laundry freshener, and even to treat bee stings! But the most common way it’s used is as a raising agent for breads and pastries. Baking soda is made in two ways: either by mining it from rivers and lakes, or by mixing Soda Ash and Carbon. Baking soda works by reacting with acid and moisture, which produces carbon dioxide gas as a by-product. That gas is what makes the bubbles in bread.',
    h2o: 'In bread, water works similarly to glue, it binds all the ingredients together into one substance. If you add too much water to bread, it will be too moist and could even cave in on itself If you add too little, the bread will be dense and crumbly.',
    flour: 'Flour is a powdery grain made by grinding up different parts of wheat kernel. Flour is used in cooking in a variety of ways, the tastiest of which is breads and pastries. If you use too much of it in bread, the bread will be dense and crumbly. If you use too little, the bread will be too wet and it may even cave in on itself.',
    heat: 'Heat describes the energy something gives off, specifically: kinetic and thermal energies. In cooking, heat is used to break down the cell walls of the flour. When you bake bread, the heat of the oven also causes the raising agent to work faster, which creates more bubbles and a golden crust.',
    timer: 'Time is the most important part of baking. Most recipes need a certain amount of time to rest, and the amount of time spent in the oven is vital to monitor. Too much and you burn your bread or dry it out, too little and it doesn’t cook.',
};